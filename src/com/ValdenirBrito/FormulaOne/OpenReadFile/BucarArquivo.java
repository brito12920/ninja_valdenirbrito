
package com.ValdenirBrito.FormulaOne.OpenReadFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.prefs.Preferences;
import javax.swing.JFileChooser;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 *
 * @author v103760 Esta classe é reponsável por buscar o arquivo na raiz e obter o path 
 * Pega o arquivo e transforma todos espaços em brancos por ; e gera um segundo arquivo formatado para um padrão de carregamento de dadps
 * A lista é convertidada em um array que faz a formatação em colunas
 * separados por ; Exemplo o array[0] reprenta a coluna hora e assim por diante
 *
 */
public class BucarArquivo {       ///-----------> Método para buscar o arquivo
 static ArrayList<String> lista = new ArrayList<>();
    public static void cria() {

        Preferences pref = Preferences.userRoot();

        String path = pref.get("DEFAULT_PATH", "");

        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setCurrentDirectory(new File(path));
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            File arq = chooser.getSelectedFile().getAbsoluteFile();
            chooser.setCurrentDirectory(arq);
            pref.put("DEFAULT_PATH", arq.getAbsolutePath());
            String caminho = arq.getPath();
            System.out.println(caminho);

            buscarArquivo(caminho);

        }
    }

    public static void buscarArquivo(String path) {  //-------------------> Método que carrega os dados de um arquivo em um a ArrayList

        try {
            FileReader arq = new FileReader(path);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // lê a primeira linha

            int pos = 0;

            while (linha != null) {

                if (pos == 0) {

                } else {
                    String valor = linha.replaceAll("\\s+", ";");   // Substitue os espaços em brancos por ;
                    lista.add(valor);

                }

                linha = lerArq.readLine(); // lê da segunda até a última linha
                pos++;

            }

            arq.close();
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
        showMessageDialog(null,"Arquivo carregado com sucesso: ");
        gerarSegundoArquivo(); //---------------> Gera um segundo arquivo
        System.out.println();
    }

    public static void gerarSegundoArquivo() {  //-----> gera um seguno arquivo na raiz do sistema
        try {
            BufferedWriter saida = new BufferedWriter(new FileWriter("Corrida2.txt"));

            for (int i = 0; i < lista.size(); i++) {
                String array[] = new String[3];

                array = lista.get(i).toString().split(";");

                saida.write(lista.get(i));
                saida.newLine();

            }
            showMessageDialog(null,"Segundo arquivo gerado com sucesso com o nome : Corrida2.txt");
            saida.close();
        } catch (IOException e) {
        }
    }

}
