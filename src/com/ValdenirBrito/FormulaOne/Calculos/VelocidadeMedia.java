
package com.ValdenirBrito.FormulaOne.Calculos;

import java.util.ArrayList;
import javax.swing.JTextPane;

/**
 *
 * @author v103760 Esta classe é reponsável por calcular a velocidade média de cada piloto
 * A lista é convertidada em um array que faz a formatação em colunas
 * separados por ; Exemplo o array[0] reprenta a coluna hora e assim por diante
 *
 */
public class VelocidadeMedia {
   
     public static void velocidadeMedia(String codigo, ArrayList lista, JTextPane painel){
     
    double valor=0;
    double media=0;
    int contador=1;
    String piloto=null;
    for(int i=0; i < lista.size();i++){
         
            String array[] = new String[3];

            array = lista.get(i).toString().split(";");
           
           if(array[1].equals(codigo)){
      
               valor += Double.parseDouble(array[6].replaceAll(",", ".")); 
               
               media = valor / contador;
               contador++;
               piloto = array[3];
           }
}
        System.out.println("Média de velocidade "+" Piloto "+piloto+" Media "+media);
        
         painel.setText(painel.getText() + "\n" + "A média de velocidade "+" do "+piloto+" foi de "+media + " metros por segundo");
}
    
    
}
