/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ValdenirBrito.FormulaOne.Calculos;

import java.util.ArrayList;
import javax.swing.JTextPane;

/**
 *
 * @author v103760
 * Esta classe é reponsável por verificar os pilotos que completaram 4 voltas
 * A lista é convertidada em um array que faz a formatação em colunas separados por ;
 * Exemplo o array[0] reprenta a coluna hora e assim por diante
 * 
 */
public class CompletouCorrida {
    
    public static void completouCorrida(ArrayList lista, JTextPane painel){
     
    ArrayList<String> completou = new ArrayList<>();
    for(int i=0; i < lista.size();i++){
         
            String array[] = new String[3];

                  array = lista.get(i).toString().split(";");
                 
           int voltas = Integer.parseInt(array[4]);
           if(voltas==4){
               completou.add(array[3]);
               
           }
}
System.out.println("Completou a corrida "+completou);
painel.setText("-------------------------PILOTOS QUE COMPLETARAM A CORRIDA-------------------------------------------------------------");
painel.setText(painel.getText() + "\n" + "Completaram a corrida "+completou);
painel.setText(painel.getText() + "\n" + "--------------------------------------------------------------------------------------------------------------------------------------------------");

}
    
}
